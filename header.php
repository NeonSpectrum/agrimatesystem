<?php
if (!isset($_SESSION['name'])) {
  header('Location: login');
}
?>
<header class="main-header">
  <!-- Logo -->
  <a href="./" class="logo">
    <!-- logo for regular state and mobile devices -->
    <span class="logo-mini"><img src="favicon/favicon-16x16.png" alt="" height="20px"></span>
    <span class="logo-lg"><b>Agrimate</b> Inc.</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" style="height:50px">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li class="dropdown notifications-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-bell-o"></i>
            <span class="label label-warning notification-count">0</span>
          </a>
          <ul class="dropdown-menu">
            <li class="header">You have <span class="notification-count">0</span> notification(s)</li>
            <li>
              <!-- inner menu: contains the actual data -->
              <ul class="menu notification-data">
              </ul>
            </li>
            <!-- <li class="footer"><a href="#">View all</a></li> -->
          </ul>
        </li>
        <li class="navbar-text" style="color:white;user-select:none">
          <img src="images/user-icon.png" class="user-image" alt="User Image">
          <span class="hidden-xs"><?php echo $_SESSION['name']; ?></span>
        </li>
        <li>
          <a href="models/logout_process.php" onclick="return confirm('Are you sure do you want to sign out?')">Sign Out</a>
        </li>
      </ul>
    </div>
  </nav>
</header>

<?php
include 'models/process_save_inv.php';
?>
