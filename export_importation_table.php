<?php
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$title[] = 'Proforma Inv No';
$title[] = 'Commercial Inv No';
$title[] = 'Date Created';
$title[] = 'Product Code';
$title[] = 'Packaging';
$title[] = 'Supplier';
$title[] = 'Origin';
$title[] = 'Quantity';
$title[] = 'Status';

$result = $con->query('SELECT * FROM `tbl_importation` JOIN tbl_imp_product ON tbl_importation.ProformaInvNo=tbl_imp_product.ProformaInvNo JOIN tbl_product ON tbl_imp_product.ProductID=tbl_product.ProductID JOIN tbl_packaging ON tbl_imp_product.PackagingID=tbl_packaging.PackagingID');
for ($i = 0; $row = $result->fetch_assoc();) {
  $dateCreated = strtotime($row['DateCreated']);

  if ($proforma_inv_no && $proforma_inv_no != $row['ProformaInvNo']
    || $commercial_inv_no && $commercial_inv_no != $row['CommercialInvNo']
    || $date && !($dateCreated >= $timestamp[0] && $dateCreated <= $timestamp[1])
    || $supplier && $supplier != getSupplierName($row['SupplierID'])
    || $origin && $origin != getOriginName($row['Origin'])
    || $status && strtoupper($status) != $row['DeliveryStatus']) {
    continue;
  }
  $data[$i][] = $row['ProformaInvNo'];
  $data[$i][] = $row['CommercialInvNo'];
  $data[$i][] = date('F d, Y', $dateCreated);
  $data[$i][] = $row['ProductName'];
  $data[$i][] = $row['Packaging'];
  $data[$i][] = getSupplierName($row['SupplierID']);
  $data[$i][] = getOriginName($row['Origin']);
  $data[$i][] = $row['Quantity'];
  $data[$i][] = $row['DeliveryStatus'];

  $i++;
}

$spreadsheet = new Spreadsheet();
$sheet       = $spreadsheet->getActiveSheet();

for ($i = 0, $cell = 'A'; $i < count($title); $i++, $cell++) {
  $sheet->setCellValue($cell . '1', $title[$i]);
}

for ($i = 0; $i < count($data); $i++) {
  if (isset($data[$i])) {
    for ($j = 0, $cell = 'A'; $j < count($data[$i]); $j++, $cell++) {
      $sheet->setCellValue($cell . ($i + 2), $data[$i][$j]);
    }
  }
}

$sheet->getStyle('A1:I1')->getFont()->setBold(true);

foreach (range('A', $sheet->getHighestDataColumn()) as $col) {
  $sheet
    ->getColumnDimension($col)
    ->setAutoSize(true);
}

$writer = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . date('F_d_Y_h_i_s_A') . ' - Importation Report.xlsx"');
$writer->save('php://output');
?>
