<?php
include 'css.php';
include 'models/sales_order_model.php';

$sox = new Sales_Order();
$sox->set_data($_GET['so']);

date_default_timezone_set('Asia/Manila');
$date  = date('j-M-y');
$so    = $_GET['so'];
$dr    = $sox->dr;
$po    = $sox->po;
$terms = $sox->terms;
?>

<body>

                <img src="images/logo.jpg" height="100" width="100" alt="">
                <table style='width:100%; line-height:30px;'>
                    <!-- table-bordered table-striped-->
                  <tbody>
                    <tr>
                      <th colspan='5' style='text-align:right; font-size: 100%; font-weight: normal'>&nbsp;<br>&nbsp;</th>
                    </tr>
                    <tr>
                      <th colspan='3' style='padding-left: 100px; text-align:left; font-size: 100%; font-weight: normal'><?php echo $sox->name; ?></th>
                      <th colspan='2' style='text-align:right; font-size: 100%; font-weight: normal'><?php echo $date; ?></th>
                    </tr>
                    <tr>
                      <th colspan='5' style='text-align:right; font-size: 100%; font-weight: normal'><?php echo sprintf('%06d', $so); ?></th>
                    </tr>
                    <tr>
                      <th colspan='5' style='text-align:right; font-size: 100%; font-weight: normal'><?php if ($dr == null) {echo '-';} else {echo $dr;}
;?></th>
                    </tr>
                    <tr>
                    <th colspan='3' style='padding-left: 100px; text-align:left; font-size: 100%; font-weight: normal'><?php echo $sox->address; ?></th>
                      <th colspan='2' style='text-align:right; font-size: 100%; font-weight: normal'><?php if ($po == null) {echo '-';} else {echo $po;}
;?></th>
                    </tr>
                    <tr>
                      <th colspan='5' style='text-align:right; font-size: 100%; font-weight: normal'><?php echo $terms; ?></th>
                    </tr>
                  </tbody>
                </table>
                <br><br>
                <table style='width:100%'>
                    <!-- table-bordered table-striped-->
                  <tbody>
                  <?php
$sox->show_product_print($so);
?>
                  <tr>
                    <th colspan='2'>&nbsp;</th>
                    <td colspan='1'>*****NOTHING FOLLOWS*****</td>
                    <th colspan='2'>&nbsp;</th>
                  </tr>
                  <tr>
                    <th colspan='5'>&nbsp;</th>
                  </tr>
                  <tr>
                    <th colspan='5'>&nbsp;</th>
                  </tr>
                  <tr>
                    <th colspan='5'>&nbsp;</th>
                  </tr>
                  <tr>
                    <th colspan='5'>&nbsp;</th>
                  </tr>
                  <tr>
                    <th colspan='5'>&nbsp;</th>
                  </tr>
                  <tr>
                    <th colspan='5'>&nbsp;</th>
                  </tr>
                  <tr>
                    <th colspan='5'>&nbsp;</th>
                  </tr>
                  <tr>
                    <th colspan='5'>&nbsp;</th>
                  </tr>
                  <tr>
                    <th colspan='5'>&nbsp;</th>
                  </tr>
                  <tr>
                    <th colspan='4'>&nbsp;</th>
                    <td colspan='1'><?php echo $sox->total; ?></td>
                  </tr>
                  <tr>
                    <th colspan='5'>&nbsp;</th>
                  </tr>
                  <tr>
                    <th colspan='4'>&nbsp;</th>
                    <td colspan='1'><?php echo $sox->total; ?></td>
                  </tr>
                  </tbody>
                </table>
</body>

<?php
include 'js.php';
?>


<script>
    window.print();
        setTimeout("closePrintView()", 1000);
    function closePrintView() {
        document.location.href = 'approved_records';
    }
</script>
