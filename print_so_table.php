<?php
include 'models/connection.php';

$customer = $_GET['customer'] ?? null;
$date     = $_GET['date'] ? explode(' - ', $_GET['date']) : null;

if ($date) {
  $timestamp[0] = strtotime($date[0]);
  $timestamp[1] = strtotime($date[1]);
}

include 'css.php';

?>

<body>
                <img src="images/logo.jpg" height="100" width="100" style="position:absolute;left:10px;top:10px" alt="">
                <center>
                  <h1>Sales Order Report</h1>
<?php
$filter = [];

if ($customer) {
  $filter[] = '<small>Customer Name: ' . $customer . '</small>';
}if ($date) {
  $filter[] = '<small>From: ' . date('F d, Y', $timestamp[0]) . ' To: ' . date('F d, Y', $timestamp[1]) . '</small>';
}

echo join('<br>', $filter);
?>
                </center>
                <br>

                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>SO #</th>
                      <th>Date</th>
                      <th>Customer Name</th>
                      <th>Product Code</th>
                      <th>Quantity</th>
                    </tr>
                  </thead>
                  <tbody>
<?php
$result = $con->query('SELECT * FROM tbl_sales_order JOIN tbl_so_product ON tbl_sales_order.SONumber=tbl_so_product.SO_ID JOIN tbl_customers ON tbl_customers.CustomerID=tbl_sales_order.CustomerID WHERE VerifiedStatus="YES" AND NotedStatus="YES"');
while ($row = $result->fetch_assoc()) {
  $dateCreated = strtotime($row['DateCreated']);

  if ($customer && $customer != strtoupper(str_replace('"', '', $row['CompanyName']))
    || $date && !($dateCreated >= $timestamp[0] && $dateCreated <= $timestamp[1])) {
    continue;
  }
  ?>
                    <tr>
                      <td><?php echo $row['SONumber']; ?></td>
                      <td><?php echo date('F d, Y', $dateCreated); ?></td>
                      <td><?php echo strtoupper(str_replace('"', '', $row['CompanyName'])); ?></td>
                      <td><?php echo $row['ProductCode']; ?></td>
                      <td><?php echo $row['Quantity']; ?></td>
                    </tr>
<?php
}
?>
                  </tbody>
                </table>
</body>

<?php
include 'js.php';
?>

<script>
// window.print();
// window.onfocus=function(){ window.close();}
</script>
