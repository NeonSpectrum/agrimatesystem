<?php
if(isset($_GET['id'])){
    include 'models/customer_model.php';
    include 'models/style_model.php';

    $cust = new Customer();
    $style = new Style();

    $cust->set_data($_GET['id']);
    
    $id = $_GET['id'];
    $name = $cust->name;
    $address = $cust->address;
    $cperson = $cust->cperson;
    $cnumber = $cust->cnumber;
    $credit = $cust->credit;
    $tin = $cust->tin;
    $industry = $cust->industry;
}
else{
    include 'models/style_model.php';
    $style = new Style();

    $id= '';
    $name = '';
    $address = '';
    $cperson = '';
    $cnumber = '';
    $credit = '';
    $tin = '';
}
?>

<style>
.loader {
  border: 4px solid #f3f3f3;
  border-radius: 50%;
  border-top: 4px solid #3498db;
  width: 12px;
  height: 12px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>

            <!-- /.card-header -->
            <div class="card-body">
                <form role="form" action="models/customer_model.php" method="post" id="imp_form">
                  <div class="form-group">
                    <div style='width:100%'>
                    <label>Company Name</label>
                    <span style='float:right;' id='cname'></span>
                    </div>
                    <input type='hidden' name='id' value='<?php echo $id; ?>'>
                    <input type='text' maxlength='50' required 
                    
                    <?php
                    if($name != ''){
                      echo ' readonly ';
                    }
                    ?>

                    onkeyup="loading('cname')" value='<?php echo $name; ?>' id='comp_name' name="name" class="form-control" placeholder='Type input here...'>
                  </div>

                  <div class="form-group">
                    <label>Company Address</label>
                    <textarea maxlength='150' onkeypress="" rows='2' style='resize:none' required name="address" class="form-control" placeholder='Type input here...'><?php echo $address; ?></textarea>
                  </div>

                  <?php
                    if($name != ''){
                  ?>    
                  
                  <div class="form-group">
                    <label>Business Style</label>
                    <input type='text' maxlength='50' required readonly value='<?php echo $industry; ?>' class="form-control" placeholder='Type input here...'>
                  </div>
                  
                  <?php
                    }
                    else{
                  ?>

                   <div class="form-group">
                    <label>Business Style</label>
                    <select required class="form-control" name='style'>
                        <?php $style->show_select(); ?>
                    </select>
                  </div>

                  <?php
                    }
                  ?>

                  <div class="form-group">
                    <label>Contact Person</label>
                    <input type='text' maxlength='50' onkeypress="return onlyAlphabets(event,this)" value='<?php echo $cperson; ?>' name="cperson" class="form-control" placeholder='Type input here...'>
                  </div>

                  <div class="form-group">
                    <label>Contact Number</label>
                    <input type='text' maxlength='11' onkeypress="return isNumberKey(event)" value='<?php echo $cnumber; ?>' name="cnumber" pattern='[0-9]{11}' class="form-control" placeholder='Example: 09xxxxxxxxx ...'>
                  </div>

                  <div class="col-md-6 form-group">
                    <label>Credit Limit</label>
                    <input type='text' maxlength='20' onkeypress="return isNumberKey(event)" value='<?php echo $credit; ?>' name="credit" class="form-control" placeholder='Type input here...'>
                  </div>

                  <div class="col-md-6 form-group">
                    <label>TIN</label>
                    <input type='text' maxlength='20' onkeypress="return isNumberKey(event)" value='<?php echo $tin; ?>' name="tin" class="form-control" placeholder='Type input here...'>
                  </div>
               
                  <button type="submit" 
                  
                  <?php
                    if($name != ''){
                        echo "
                            name='update'
                        ";
                    }
                    else{
                        echo "
                            name='save'
                        ";
                    }
                   ?>
                  
                   id='submit' class="btn btn-success" style="float:right; margin-top:2px; margin-right:10px"><i class='fa fa-save'></i> &nbsp;Save Details</button>
                </form>

            </div>
            <!-- /.card-body -->

<script>
function onlyAlphabets(e, t) {

if (window.event) {
    var charCode = window.event.keyCode;
}
else if (e) {
    var charCode = e.which;
}
else { return true; }
if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 32 || charCode == 46 || charCode == 8)
    return true;
else
    return false;

}

function loading(id){
		document.getElementById(id).innerHTML = "<div style='float:right; font-weight:bold'>&nbsp;&nbsp;&nbsp;Loading</div><div class='loader' style='float:right'></div>"; // Hide the image after the response from the server
		 $('#'+id).attr("style", "color: green");
	}

    function isNumberKey(evt){
         	var charCode = (evt.which) ? evt.which : event.keyCode
         	if (charCode > 31 && (charCode < 48 || charCode > 57))
            	return false;
         	return true;
    }

  function check_comp_name() {
		
		var str = $('#comp_name').val();
		
	    if (str.length == 0) { 
	        document.getElementById("cname").innerHTML = "";
	        return;
	    } 
		else {
	        var xmlhttp = new XMLHttpRequest();
	        xmlhttp.onreadystatechange = function() {
	            if (this.readyState == 4 && this.status == 200) {
	                document.getElementById("cname").innerHTML = this.responseText;
					if(this.responseText == " &nbsp; Company name already exists! "){
						$("#cname").attr("style", "color: red; font-weight: bold; float:right;");
						$("#submit").attr("disabled", true);
					}
					else{
						$("#cname").attr("style", "color: green; font-weight: bold; float:right;");
						$("#submit").attr("disabled", false);
					}
	            }
	        };
	        xmlhttp.open("GET", "models/validate_ajax.php?q_cname=" + str, true);
	        xmlhttp.send();
	    }
	}

var typingTimer;                //timer identifier
var doneTypingInterval = 1000;  //time in ms, 5 second for example
var $input_cname = $('#comp_name');

//on keyup, start the countdown
$input_cname.on('keyup', function () {
  $("#submit").attr("disabled", true);
  clearTimeout(typingTimer);
  typingTimer = setTimeout(check_comp_name, doneTypingInterval);
});
</script>