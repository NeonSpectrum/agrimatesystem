<?php
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$title[] = 'CMO';
$title[] = 'Area';
$title[] = 'Customer Name';
$title[] = 'SO #';
$title[] = 'Date';
$title[] = 'SI #';
$title[] = 'SI Date';
$title[] = 'DR #';
$title[] = 'DR Date';
$title[] = 'Product';
$title[] = 'Qty';
$title[] = 'Packaging';
$title[] = 'Unit Price';
$title[] = 'Sum of Total';

$result = $con->query('SELECT * FROM tbl_sales_order JOIN tbl_so_product ON tbl_sales_order.SONumber=tbl_so_product.SO_ID JOIN tbl_customers ON tbl_sales_order.CustomerID=tbl_customers.CustomerID');
for ($i = 0; $row = $result->fetch_assoc();) {
  $dateCreated = strtotime($row['DateCreated']);

  if ($cmo && !in_array($cmo, getCMO($row['CustomerID']))
    || $area && $area != $row['Address']
    || $customer_name && $customer_name != str_replace('"', '', $row['CompanyName'])
    || $prod_code && $prod_code != $row['ProductCode']
    || $date && !($dateCreated >= $timestamp[0] && $dateCreated <= $timestamp[1])) {
    continue;
  }
  $actualProduct = getActualProduct($row['ProductCode']);

  $data[$i][] = join(', ', getCMO($row['CustomerID']));
  $data[$i][] = $row['Address'];
  $data[$i][] = $row['CompanyName'];
  $data[$i][] = $row['SONumber'];
  $data[$i][] = date('F d, Y', $dateCreated);
  $data[$i][] = $row['SI_Number'];
  $data[$i][] = $row['SI_Date'] ? date('F d, Y', strtotime($row['SI_Date'])) : '';
  $data[$i][] = $row['DR_Number'];
  $data[$i][] = $row['DR_Date'] ? date('F d, Y', strtotime($row['DR_Date'])) : '';
  $data[$i][] = $actualProduct['ProductDesc'];
  $data[$i][] = $row['Quantity'];
  $data[$i][] = $actualProduct['Packaging'];
  $data[$i][] = number_format($row['Price'], 2, '.', ',');
  $data[$i][] = number_format($row['Amount'], 2, '.', ',');

  $rebateResult = getRebate($row['SONumber']);
  while ($rebateRow = $rebateResult->fetch_assoc()) {
    $i++;
    $data[$i][] = '';
    $data[$i][] = '';
    $data[$i][] = '';
    $data[$i][] = 'REBATES';
    $data[$i][] = '';
    $data[$i][] = '';
    $data[$i][] = '';
    $data[$i][] = '';
    $data[$i][] = '';
    $data[$i][] = '';
    $data[$i][] = '';
    $data[$i][] = '';
    $data[$i][] = '';
    $data[$i][] = number_format($rebateRow['Amount'], 2, '.', ',');
  }

  $i++;
}

$spreadsheet = new Spreadsheet();
$sheet       = $spreadsheet->getActiveSheet();

for ($i = 0, $cell = 'A'; $i < count($title); $i++, $cell++) {
  $sheet->setCellValue($cell . '1', $title[$i]);
}

for ($i = 0; $i < count($data); $i++) {
  if (isset($data[$i])) {
    for ($j = 0, $cell = 'A'; $j < count($data[$i]); $j++, $cell++) {
      $sheet->setCellValue($cell . ($i + 2), $data[$i][$j]);
    }
  }
}

$sheet->getStyle('A1:N1')->getFont()->setBold(true);

foreach (range('A', $sheet->getHighestDataColumn()) as $col) {
  $sheet
    ->getColumnDimension($col)
    ->setAutoSize(true);
}

$writer = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . date('F_d_Y_h_i_s_A') . ' - Invoice Report.xlsx"');
$writer->save('php://output');
?>
