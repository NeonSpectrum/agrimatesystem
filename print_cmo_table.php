<?php
include 'models/connection.php';
$proforma_inv_no   = $_GET['proforma_inv_no'] ?? null;
$commercial_inv_no = $_GET['commercial_inv_no'] ?? null;
$date              = $_GET['date'] ? explode(' - ', $_GET['date']) : null;
$supplier          = $_GET['supplier'] ?? null;
$origin            = $_GET['origin'] ?? null;
$status            = $_GET['status'] ?? null;

if ($date) {
  $timestamp[0] = strtotime($date[0]);
  $timestamp[1] = strtotime($date[1]);
}

if (isset($_GET['excel'])) {
  include 'export_importation_table.php';
  die();
}

include 'css.php';

?>

<body>
                <img src="images/logo.jpg" height="100" width="100" style="position:absolute;left:10px;top:10px" alt="">
                <center>
                  <h1>Inventory Report</h1>
<?php
$filter = [];

if ($proforma_inv_no) {
  $filter[] = '<small>Proforma Inv No: ' . $proforma_inv_no . '</small>';
}
if ($commercial_inv_no) {
  $filter[] = '<small>Commercial Inv No: ' . $commercial_inv_no . '</small>';
}
if ($date) {
  $filter[] = '<small>From: ' . date('F d, Y', $timestamp[0]) . ' To: ' . date('F d, Y', $timestamp[1]) . '</small>';
}
if ($supplier) {
  $filter[] = '<small>Supplier: ' . $supplier . '</small>';
}
if ($origin) {
  $filter[] = '<small>Origin: ' . $origin . '</small>';
}
if ($status) {
  $filter[] = '<small>Status: ' . $status . '</small>';
}

echo join('<br>', $filter);
?>
                </center>
                <br>

                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
<?php if (!$proforma_inv_no): ?>
                      <th>Proforma Inv No</th>
<?php endif;?>
<?php if (!$commercial_inv_no): ?>
                      <th>Commercial Inv No</th>
<?php endif;?>
                      <th>Date Created</th>
                      <th>Product Code</th>
                      <th>Packaging</th>
<?php if (!$supplier): ?>
                      <th>Supplier</th>
<?php endif;?>
<?php if (!$origin): ?>
                      <th>Origin</th>
<?php endif;?>
                      <th>Quantity</th>
<?php if (!$status): ?>
                      <th>Status</th>
<?php endif;?>
                    </tr>
                  </thead>
                  <tbody>
<?php
$result = $con->query('SELECT * FROM `tbl_importation` JOIN tbl_imp_product ON tbl_importation.ProformaInvNo=tbl_imp_product.ProformaInvNo JOIN tbl_product ON tbl_imp_product.ProductID=tbl_product.ProductID JOIN tbl_packaging ON tbl_imp_product.PackagingID=tbl_packaging.PackagingID');
while ($row = $result->fetch_assoc()) {
  $dateCreated = strtotime($row['DateCreated']);

  if ($proforma_inv_no && $proforma_inv_no != $row['ProformaInvNo']
    || $commercial_inv_no && $commercial_inv_no != $row['CommercialInvNo']
    || $date && !($dateCreated >= $timestamp[0] && $dateCreated <= $timestamp[1])
    || $supplier && $supplier != getSupplierName($row['SupplierID'])
    || $origin && $origin != getOriginName($row['Origin'])
    || $status && strtoupper($status) != $row['DeliveryStatus']) {
    continue;
  }
  ?>
                    <tr>
<?php if (!$proforma_inv_no): ?>
                      <td><?php echo $row['ProformaInvNo']; ?></td>
<?php endif;?>
<?php if (!$commercial_inv_no): ?>
                      <td><?php echo $row['CommercialInvNo']; ?></td>
<?php endif;?>
                      <td><?php echo date('F d, Y', $dateCreated); ?></td>
                      <td><?php echo $row['ProductName']; ?></td>
                      <td><?php echo $row['Packaging']; ?></td>
<?php if (!$supplier): ?>
                      <td><?php echo getSupplierName($row['SupplierID']); ?></td>
<?php endif;?>
<?php if (!$origin): ?>
                      <td><?php echo getOriginName($row['Origin']); ?></td>
<?php endif;?>
                      <td><?php echo $row['Quantity']; ?></td>
<?php if (!$status): ?>
                      <td><?php echo $row['DeliveryStatus']; ?></td>
<?php endif;?>
                    </tr>
<?php
}
?>
                  </tbody>
                </table>
</body>

<?php
include 'js.php';
?>

<?php
/**
 * @param $id
 * @return mixed
 */
function getSupplierName($id) {
  global $con;
  return $con->query("SELECT CompanyName FROM tbl_supplier WHERE SupplierID='{$id}'")->fetch_assoc()['CompanyName'];
}

/**
 * @param $id
 * @return mixed
 */
function getOriginName($id) {
  global $con;
  return $con->query("SELECT Origin FROM tbl_origin WHERE OriginID='{$id}'")->fetch_assoc()['Origin'];
}
?>

<script>
// window.print();
// window.onfocus=function(){ window.close();}
</script>
