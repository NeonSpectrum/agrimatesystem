<?php
require 'vendor/autoload.php';
require 'models/connection.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$cmo                       = $_GET['cmo'];
$customer_name             = $_GET['customer_name'];
$collection_receipt_number = $_GET['collection_receipt_number'];
$check_number              = $_GET['check_number'];
$date                      = $_GET['date'] ? explode(' - ', $_GET['date']) : null;

if ($date) {
  $timestamp[0] = strtotime($date[0]);
  $timestamp[1] = strtotime($date[1]);
}

$title[] = 'CMO';
$title[] = 'Customer Name';
$title[] = 'Balance';
$title[] = 'Total Amount Paid';
$title[] = 'Collection Receipt #';
$title[] = 'Check Number';
$title[] = 'Date';

$result = $con->query('SELECT * FROM tbl_sales_order JOIN tbl_so_product ON tbl_sales_order.SONumber=tbl_so_product.SO_ID JOIN tbl_customers ON tbl_sales_order.CustomerID=tbl_customers.CustomerID JOIN tbl_payment ON tbl_sales_order.SONumber=tbl_payment.SONumber');
for ($i = 0; $row = $result->fetch_assoc(); $i++) {
  $dateCreated = strtotime($row['DateCreated']);

  if ($cmo && !in_array($cmo, getCMO($row['CustomerID']))
    || $customer_name && $customer_name != str_replace('"', '', $row['CompanyName'])
    || $date && !($dateCreated >= $timestamp[0] && $dateCreated <= $timestamp[1])) {
    continue;
  }

  $data[$i][] = join(', ', getCMO($row['CustomerID']));
  $data[$i][] = $row['CompanyName'];
  $data[$i][] = number_format($row['Balance'], 2, '.', ',');
  $data[$i][] = number_format($row['TotalAmount'], 2, '.', ',');
  $data[$i][] = $row['CR_Number'];
  $data[$i][] = $row['CheckNumber'];
  $data[$i][] = date('F d, Y', $dateCreated);
}

$spreadsheet = new Spreadsheet();
$sheet       = $spreadsheet->getActiveSheet();

for ($i = 0, $cell = 'A'; $i < count($title); $i++, $cell++) {
  $sheet->setCellValue($cell . '1', $title[$i]);
}

for ($i = 0; $i < count($data); $i++) {
  if (isset($data[$i])) {
    for ($j = 0, $cell = 'A'; $j < count($data[$i]); $j++, $cell++) {
      $sheet->setCellValue($cell . ($i + 2), $data[$i][$j]);
    }
  }
}

$sheet->getStyle('A1:G1')->getFont()->setBold(true);

foreach (range('A', $sheet->getHighestDataColumn()) as $col) {
  $sheet
    ->getColumnDimension($col)
    ->setAutoSize(true);
}

$writer = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . date('F_d_Y_h_i_s_A') . ' - Invoice Report.xlsx"');
$writer->save('php://output');

/**
 * @param $id
 */
function getCMO($id) {
  global $con;

  $list = [];

  $result = $con->query("SELECT * FROM `tbl_cmo` JOIN tbl_cmo_cust ON tbl_cmo.CMO_ID=tbl_cmo_cust.CMO_ID WHERE CustomerID='{$id}'");

  while ($row = $result->fetch_assoc()) {
    $list[] = $row['FullName'];
  }

  return $list;
}

?>
