<?php
include 'models/connection.php';

$target        = $_GET['target'] ?? 0;
$cmo           = $_GET['cmo'] ?? null;
$area          = $_GET['area'] ?? null;
$customer_name = $_GET['customer_name'] ?? null;
$prod_code     = $_GET['prod_code'] ?? null;
$date          = $_GET['date'] ? explode(' - ', $_GET['date']) : null;

if ($date) {
  $timestamp[0] = strtotime($date[0]);
  $timestamp[1] = strtotime($date[1]);
}

if (isset($_GET['excel'])) {
  include 'export_invoice_table.php';
  die();
}

include 'css.php';

?>

<body>
                <img src="images/logo.jpg" height="100" width="100" style="position:absolute;left:10px;top:10px" alt="">
                <center>
                  <h1>Sales Invoice Report</h1>
<?php
$filter = [];

if ($cmo) {
  $filter[] = '<small>CMO: ' . $cmo . '</small>';
}
if ($area) {
  $filter[] = '<small>Area: ' . $area . '</small>';
}
if ($customer_name) {
  $filter[] = '<small>Customer Name: ' . $customer_name . '</small>';
}
if ($prod_code) {
  $filter[] = '<small>Product Code: ' . $prod_code . '</small>';
}
if ($date) {
  $filter[] = '<small>From: ' . date('F d, Y', $timestamp[0]) . ' To: ' . date('F d, Y', $timestamp[1]) . '</small>';
}
if ($target) {
  $filter[] = '<small>Target: ' . number_format($target, 2, '.', ',') . ' </small>';
}

echo join('<br>', $filter);
?>
                </center>
                <br>

                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>CMO</th>
                      <th>Area</th>
                      <th>Customer Name</th>
                      <th>SO #</th>
                      <th>Date</th>
                      <th>SI #</th>
                      <th>SI Date</th>
                      <th>DR #</th>
                      <th>DR Date</th>
                      <th>Product</th>
                      <th>Qty</th>
                      <th>Packaging</th>
                      <th>Unit Price</th>
                      <th>Sum of Total</th>
                    </tr>
                  </thead>
                  <tbody>
<?php
$gross = $rebate = $credit_memo = $debit_memo = 0;

$result = $con->query('SELECT * FROM tbl_sales_order JOIN tbl_so_product ON tbl_sales_order.SONumber=tbl_so_product.SO_ID JOIN tbl_customers ON tbl_sales_order.CustomerID=tbl_customers.CustomerID');
while ($row = $result->fetch_assoc()) {
  $dateCreated = strtotime($row['DateCreated']);

  if ($cmo && !in_array($cmo, getCMO($row['CustomerID']))
    || $area && $area != $row['Address']
    || $customer_name && $customer_name != str_replace('"', '', $row['CompanyName'])
    || $prod_code && $prod_code != $row['ProductCode']
    || $date && !($dateCreated >= $timestamp[0] && $dateCreated <= $timestamp[1])) {
    continue;
  }

  $actualProduct = getActualProduct($row['ProductCode']);

  $debit_memo += getDebitMemo($row['SONumber']);
  $credit_memo += getCreditMemo($row['SONumber']);
  $gross += $row['Amount'];
  ?>
                    <tr>
                      <td><?php echo join(', ', getCMO($row['CustomerID'])); ?></td>
                      <td><?php echo $row['Address']; ?></td>
                      <td><?php echo $row['CompanyName']; ?></td>
                      <td><?php echo $row['SONumber']; ?></td>
                      <td><?php echo date('F d, Y', $dateCreated); ?></td>
                      <td><?php echo $row['SI_Number']; ?></td>
                      <td><?php echo $row['SI_Date'] ? date('F d, Y', strtotime($row['SI_Date'])) : ''; ?></td>
                      <td><?php echo $row['DR_Number']; ?></td>
                      <td><?php echo $row['DR_Date'] ? date('F d, Y', strtotime($row['DR_Date'])) : ''; ?></td>
                      <td><?php echo $actualProduct['ProductDesc']; ?></td>
                      <td><?php echo $row['Quantity']; ?></td>
                      <td><?php echo $actualProduct['Packaging']; ?></td>
                      <td><?php echo number_format($row['Price'], 2, '.', ','); ?></td>
                      <td><?php echo number_format($row['Amount'], 2, '.', ','); ?></td>
                    </tr>
<?php
$rebateResult = getRebate($row['SONumber']);
  while ($rebateRow = $rebateResult->fetch_assoc()) {
    $rebate += $rebateRow['Amount'];
    ?>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>REBATES</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td><?php echo number_format($rebateRow['Amount'], 2, '.', ','); ?></td>
                    </tr>
<?php
}
}
?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th colspan="13" style="text-align:right">Total:</th>
                      <th style="text-align:right"><?php echo number_format($gross, 2, '.', ','); ?></th>
                    </tr>
                  </tfoot>
                </table>
                <div style="position:absolute;bottom:5px;right:5px">
                  <table width="300px">
                    <tr style="font-weight:bold">
                      <td>GROSS SALES</td>
                      <td align="right"><?php echo number_format($gross, 2, '.', ','); ?></td>
                    </tr>
                    <tr style="font-weight:bold">
                      <td>REBATES</td>
                      <td align="right"><?php echo number_format($rebate, 2, '.', ','); ?></td>
                    </tr>
                    <tr style="font-weight:bold">
                      <td>CREDIT MEMO</td>
                      <td align="right"><?php echo number_format($credit_memo, 2, '.', ','); ?></td>
                    </tr>
                    <tr style="border-bottom: 1px solid black;font-weight:bold">
                      <td>DEBIT MEMO</td>
                      <td align="right"><?php echo number_format($debit_memo, 2, '.', ','); ?></td>
                    </tr>
                    <tr style="font-weight:bold">
                      <td>NET SALES</td>
                      <td align="right"><?php echo number_format($gross - ($rebate + $credit_memo + $debit_memo), 2, '.', ','); ?></td>
                    </tr>
                    <tr style="font-weight:bold">
                      <td>TARGET</td>
                      <td align="right" style="border-bottom: 1px solid black"><?php echo number_format($target, 2, '.', ','); ?></td>
                    </tr>
                    <tr style="font-weight:bold">
                      <td>PERCENTAGE</td>
                      <td align="right"><?php echo (($gross - ($rebate + $credit_memo + $debit_memo)) / $target) * 100 . '%' ?></td>
                    </tr>
                  </table>
                </div>
</body>

<?php
include 'js.php';
?>

<?php
/**
 * @param $id
 * @return mixed
 */
function getCMO($id) {
  global $con;

  $list = [];

  $result = $con->query("SELECT * FROM `tbl_cmo` JOIN tbl_cmo_cust ON tbl_cmo.CMO_ID=tbl_cmo_cust.CMO_ID WHERE CustomerID='{$id}'");

  while ($row = $result->fetch_assoc()) {
    $list[] = $row['FullName'];
  }

  return $list;
}

/**
 * @param $id
 * @return mixed
 */
function getActualProduct($prod_code) {
  global $con;

  return $con->query("SELECT * FROM tbl_actual_product WHERE ProductCode='{$prod_code}'")->fetch_assoc();
}

/**
 * @param $id
 * @return mixed
 */
function getCreditMemo($id) {
  global $con;

  return $con->query("SELECT SUM(Total) FROM tbl_credit WHERE SONumber='{$id}'")->fetch_assoc()['Total'] ?? 0;
}

/**
 * @param $id
 * @return mixed
 */
function getDebitMemo($id) {
  global $con;

  return $con->query("SELECT SUM(Total) FROM tbl_debit WHERE SONumber='{$id}'")->fetch_assoc()['Total'] ?? 0;
}

/**
 * @param $id
 * @return mixed
 */
function getRebate($id) {
  global $con;

  return $con->query('SELECT * FROM tbl_rebate JOIN tbl_rebate_bd ON tbl_rebate.RebateNumber=tbl_rebate_bd.RebateNumber');
}
?>

<script>
// window.print();
// window.onfocus=function(){ window.close();}
</script>
