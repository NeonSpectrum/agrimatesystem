<script>
// window.print();
// window.onfocus=function(){ window.close();}
</script>

<?php
include 'models/connection.php';
include 'css.php';

$payee_name    = $_GET['payee_name'] ?? null;
$ref_no        = $_GET['ref_no'] ?? null;
$check_no      = $_GET['check_no'] ?? null;
$company       = $_GET['company'] ?? null;
$date          = $_GET['date'] ? explode(' - ', $_GET['date']) : null;
$account_title = $_GET['account_title'] ?? null;
$bir           = $_GET['bir'] ?? null;
$description   = $_GET['description'] ?? null;
$bankname      = $_GET['bankname'] ?? null;

if ($date) {
  $timestamp[0] = strtotime($date[0]);
  $timestamp[1] = strtotime($date[1]);
}
?>

<body>
                <img src="images/logo.jpg" height="100" width="100" style="position:absolute;left:10px;top:10px" alt="">
                <center>
                  <h1>Check Voucher Report</h1>
<?php
$filter = [];

if ($payee_name) {
  $filter[] = '<small>Payee Name: ' . $payee_name . '</small>';
}
if ($ref_no) {
  $filter[] = '<small>AP Reference No: ' . $ref_no . '</small>';
}
if ($check_no) {
  $filter[] = '<small>Check No: ' . $check_no . '</small>';
}
if ($company) {
  $filter[] = '<small>Company: ' . $company . '</small>';
}
if ($account_title) {
  $filter[] = '<small>Account Title: ' . $account_title . '</small>';
}
if ($bir) {
  $filter[] = '<small>BIR: ' . $bir . '</small>';
}
if ($description) {
  $filter[] = '<small>Description: ' . $description . '</small>';
}
if ($bankname) {
  $filter[] = '<small>Bank Name: ' . $bankname . '</small>';
}
if ($date) {
  $filter[] = '<small>From: ' . date('F d, Y', $timestamp[0]) . ' To: ' . date('F d, Y', $timestamp[1]) . '</small>';
}

echo join('<br>', $filter);
?>
                </center>
                <br>

                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="text-align:center">Payee Name</th>
                      <th style="text-align:center">AP Reference No</th>
                      <th style="text-align:center">Check No</th>
                      <th style="text-align:center">Company</th>
                      <th style="text-align:center">Description</th>
                      <th style="text-align:center">Account Title</th>
                      <th style="text-align:center">BIR</th>
                      <th style="text-align:center">Date</th>
                      <th style="text-align:center">Amount</th>
                    </tr>
                  </thead>
                  <tbody>
<?php
$total  = 0;
$result = $con->query('SELECT * FROM tbl_ap JOIN tbl_payee ON tbl_payee.PayeeID=tbl_ap.Payee JOIN tbl_company ON tbl_company.CompanyID=tbl_ap.Company JOIN tbl_ap_particular ON tbl_ap.AP_ID=tbl_ap_particular.AP_ID');
while ($row = $result->fetch_assoc()) {
  $dateCreated = strtotime($row['Date']);

  if ($payee_name && $payee_name != $row['PayeeName']
    || $ref_no && $ref_no != $row['APRefNumber']
    || $check_no && $check_no != $row['CheckNumber']
    || $company && $company != $row['Company']
    || $account_title && $account_title != getAccountTitle($row['AccountTitle'])
    || $bir && $bir != $row['BIR']
    || $description && $description != $row['Description']
    || $bankname && $bankname != $row['BankName']
    || $date && !($dateCreated >= $timestamp[0] && $dateCreated <= $timestamp[1])) {
    continue;
  }

  $total += $row['Amount'];
  ?>
                    <tr>
                      <td><?php echo $row['PayeeName']; ?></td>
                      <td><?php echo $row['APRefNumber']; ?></td>
                      <td><?php echo $row['CheckNumber']; ?></td>
                      <td><?php echo $row['CompanyName']; ?></td>
                      <td><?php echo $row['Description']; ?></td>
                      <td><?php echo getAccountTitle($row['AccountTitle']); ?></td>
                      <td><?php echo date('F d, Y', $dateCreated); ?></td>
                      <td><?php echo $row['BIR']; ?></td>
                      <td align="right"><?php echo number_format($row['Amount'], 2, '.', ','); ?></td>
                    </tr>
<?php
}
?>
                  </tbody>
                  <tfoot>
                    <th colspan="8" style="text-align:right">Total:</th>
                    <th style="text-align:right"><?php echo number_format($total, 2, '.', ','); ?></th>
                  </tfoot>
                </table>
</body>

<?php
include 'js.php';
?>

<?php
/**
 * @param $id
 * @return mixed
 */
function getAccountTitle($id) {
  global $con;
  return $con->query("SELECT tbl_account_title.AccountTitle FROM tbl_ap_particular JOIN tbl_account_title ON tbl_ap_particular.AccountTitle=tbl_account_title.AT_ID WHERE AP_ID='{$id}'")->fetch_assoc()['AccountTitle'];
}
?>
