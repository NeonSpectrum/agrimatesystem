<?php
if (isset($_POST['submit'])) {
  $username = $_POST['username'];
  $password = $_POST['password'];

  include 'connection.php';

  $stmt = $con->prepare('
    SELECT LastName, FirstName, UserID, Redirection FROM tbl_users JOIN tbl_roles ON tbl_users.RoleID=tbl_roles.RoleID WHERE Username=? AND `Password`=?
  ');
  $stmt->bind_param('ss', $username, $password);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($lname, $fname, $uid, $redirection);
  $stmt->fetch();

  if ($stmt->num_rows > 0) {
    //session variables
    session_start();
    $_SESSION['user_id'] = $uid;
    $_SESSION['name']    = $fname . ' ' . $lname;

    //redirections
    header('location: ../' . $redirection);
  } else {
    header('location: ../login?error');
  }
}
?>
