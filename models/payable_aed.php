<?php
if(isset($_POST['save'])){
    include 'connection.php';
    
    $inv = $_POST['prof_inv_no'];
    $bank = $_POST['bank'];
    $bankaddress = $_POST['bankaddress'];
    $banknumber = $_POST['banknumber'];
    $swift = $_POST['swiftcode'];
    $ivan = $_POST['ivannum'];
    $reminders = $_POST['reminder'];

    $stmt = $con->prepare('SELECT `PayableID` FROM `tbl_payables` WHERE ProformaInvNo=?');
    $stmt->bind_param('s', $inv);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($payid);
    if($stmt->num_rows > 0){//check if existing record -> update
        $con1 = new mysqli($server, $user, $pw, $db);

        $upd = $con1->prepare('UPDATE `tbl_payables` SET `BankName`=?,`BankAddress`=?,`AccountNumber`=?,`SwiftCode`=?,`IVAN_Number`=?,`Reminder`=? WHERE `ProformaInvNo`=?');
        $upd->bind_param('sssssss', $bank, $bankaddress, $banknumber, $swift, $ivan, $reminders, $inv);
        $upd->execute();
    }
    else{//check if new record -> insert
        $con1 = new mysqli($server, $user, $pw, $db);

        $upd = $con1->prepare('INSERT INTO `tbl_payables`(`ProformaInvNo`, `BankName`, `BankAddress`, `AccountNumber`, `SwiftCode`, `IVAN_Number`, `Reminder`) VALUES (?, ?, ?, ?, ?, ?, ?)');
        $upd->bind_param('sssssss', $inv, $bank, $bankaddress, $banknumber, $swift, $ivan, $reminders);
        $upd->execute();
    }

    header('location: ../finance?success');
}
elseif(isset($_POST['payment'])){
    include 'connection.php';

    $pid = $_POST['pid'];
    $bank = $_POST['bank'];
    $amtpaid = $_POST['paid'];
    $date = $_POST['date_paid'];
    $charge = $_POST['charge'];
    $rate = $_POST['rate'];
    $php = str_replace(',', '', $_POST['phpamt']);

    $stmt = $con->prepare('INSERT INTO `tbl_paid`(`PayableID`, `DatePaid`, `Bank`, `AmountPaid`, `BankCharges`, `Rate`, `PHPAmount`) VALUES (?, ?, ?, ?, ?, ?, ?)');
    $stmt->bind_param('issdddd', $pid, $date, $bank, $amtpaid, $charge, $rate, $php);
    $stmt->execute();
    $stmt->close();
    $con->close();

    //get invoice from pid
    $con = new mysqli($server, $user, $pw, $db);
    $stmt = $con->prepare('SELECT ProformaInvNo FROM tbl_payables WHERE PayableID=?');
    $stmt->bind_param('i', $pid);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($inv);
    $stmt->fetch();
    $stmt->close();
    $con->close();

    $con = new mysqli($server, $user, $pw, $db);
    $stmt = $con->prepare('UPDATE `tbl_importation` SET `Balance`=Balance-? WHERE `ProformaInvNo`=?');
    $stmt->bind_param('ds', $amtpaid, $inv);
    if($stmt->execute()){
        header('location: ../finance?paid');
    }
}
elseif(isset($_POST['deduction'])){
    $desc = $_POST['desc'];
    $amt = $_POST['amount'];
    $inv = $_POST['inv'];
    $ded = str_replace(',', '', $_POST['total']);

    //get payableID from Inv
    include 'connection.php';     

    foreach($desc as $key => $d){
        include 'connection.php';

        if($d != NULL &&
        $amt[$key] != NULL){
          
        $a = $amt[$key];
          
          //save deductions
          $stmt = $con->prepare('INSERT INTO `tbl_payable_deductions`(`Description`, `Amount`, `ProfInvNum`) VALUES (?, ?, ?)');
          $stmt->bind_param('sds', $d, $a, $inv);
          $stmt->execute();
          $stmt->close();
          $con->close();
        }

    }

    include 'connection.php';

    $stmtx = $con->prepare('UPDATE `tbl_importation` SET `Balance`=Balance-?, `StatusDeduction`=?,`DeductedAmount`=? WHERE `ProformaInvNo`=?');
    $stat = 'YES';
    $stmtx->bind_param('dsds', $ded, $stat, $ded, $inv);
    $stmtx->execute();
  
    header('location: ../finance?deductions');
}
elseif(isset($_GET['no_ded'])){
    $id = $_GET['no_ded'];
    
    //update deducted status as YES
    include 'connection.php';

    $stmt = $con->prepare('UPDATE `tbl_importation` SET `StatusDeduction`=?,`DeductedAmount`=? WHERE `ProformaInvNo`=?');
    $stat = 'YES';
    $ded = 0;
    $stmt->bind_param('sds', $stat, $ded, $id);
    $stmt->execute();
    $stmt->close();
    $con->close();
    
        header('location: ../finance?deductions');
}
?>