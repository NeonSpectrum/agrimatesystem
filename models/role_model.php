<?php
@session_start();
include __DIR__ . '/connection.php';

class Role {

  public function __construct() {
    global $con;

    $this->uid = $_SESSION['user_id'] ?? null;

    if ($this->uid) {
      $row = $con->query("SELECT * FROM tbl_users JOIN tbl_roles ON tbl_users.RoleID=tbl_roles.RoleID WHERE UserID='{$this->uid}'")->fetch_assoc();

      $this->roleID      = $row['RoleID'];
      $this->roleName    = $row['RoleName'];
      $this->redirection = $row['Redirection'];
    }
  }

  /**
   * @param $id
   */
  public function has(...$names) {
    if ($this->uid && ($this->roleID == 1 || in_array($this->roleName, $names))) {
      return true;
    } else {
      return false;
    }
  }
}
?>
