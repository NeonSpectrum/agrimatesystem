<?php
if(isset($_POST['save'])){ //new record
    include 'connection.php';

    $name = $_POST['name'];
    $address = $_POST['address'];
    $cperson = $_POST['cperson'];
    $cnumber = $_POST['cnumber'];
    $credit = $_POST['credit'];
    $tin = $_POST['tin'];
    $style = $_POST['style'];
   
    $stmt = $con->prepare('INSERT INTO `tbl_customers`(`CompanyName`, `CompanyAddress`, `BusinessStyle`, `ContactPerson`, `ContactNumber`, `CreditLimit`, `TIN`) VALUES (?, ?, ?, ?, ?, ?, ?)');
    $stmt->bind_param('ssissds', $name, $address, $style, $cperson, $cnumber, $credit, $tin);
    if($stmt->execute()){
        header('location: ../customer?success');
    }
}
elseif(isset($_POST['update'])){ //update record
    include 'connection.php';

    $id = $_POST['id'];
    $name = $_POST['name'];
    $address = $_POST['address'];
    $cperson = $_POST['cperson'];
    $cnumber = $_POST['cnumber'];
    $credit = $_POST['credit'];
    $tin = $_POST['tin'];

    $stmt = $con->prepare('UPDATE `tbl_customers` SET `CompanyName`=?,`CompanyAddress`=?,`ContactPerson`=?,`ContactNumber`=?,`CreditLimit`=?,`TIN`=? WHERE CustomerID=?');
    $stmt->bind_param('ssssdsi', $name, $address, $cperson, $cnumber, $credit, $tin, $id);
    if($stmt->execute()){
        header('location: ../customer?edited');
    }
}
elseif(isset($_GET['id_delete'])){
    include 'connection.php';

    $id = $_GET['id_delete'];
    $d = 'YES';
    $stmt = $con->prepare('UPDATE `tbl_customers` SET `Deleted`=? WHERE `CustomerID`=?');
    $stmt->bind_param('si', $d, $id);
    if($stmt->execute()){
        header('location: ../customer?deleted');
    }
}
?>