<?php
include 'customer_aed.php';

class Customer {
    public $name;
    public $address;
    public $cperson;
    public $cnumber;
    public $credit;
    public $tin;
    public $industry;

    public function set_data($id){
        include 'models/connection.php';

        $stmt = $con->prepare('SELECT `CompanyName`, `CompanyAddress`, `ContactPerson`, `ContactNumber`, `CreditLimit`, `TIN`, bs.BusinessStyle FROM `tbl_customers` c
        JOIN tbl_business_style bs ON bs.BS_ID=c.BusinessStyle
        WHERE `CustomerID`=?');
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($name, $address, $cperson, $cnumber, $credit, $tin, $ind);
        $stmt->fetch();

        //assign local variable
        $this->name = $name;
        $this->address = $address;
        $this->cperson = $cperson;
        $this->cnumber = $cnumber;
        $this->credit = $credit;
        $this->tin = $tin;
        $this->industry = $ind;
    }

    public function show_data(){
        include 'models/connection.php';
        $stmt = $con->prepare('SELECT CustomerID, `CompanyName`, `CompanyAddress` FROM `tbl_customers` WHERE Deleted=?');
        $d = 'NO';
        $stmt->bind_param('s', $d);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($cust_id, $cname, $address);
        if($stmt->num_rows > 0){
            while($stmt->fetch()){
                $cname = strtoupper(str_replace('"','', $cname));
                $address = strtoupper(str_replace('"', '', $address));
                echo "
                <tr>
                    <td>$cname</td>
                    <td>$address</td>
                    <td>";
                    
                    $con1 = new mysqli($server, $user, $pw, $db);
                    $cmo = $con1->prepare('SELECT FullName FROM tbl_cmo_cust cc JOIN tbl_cmo cm ON cc.CMO_ID=cm.CMO_ID WHERE cc.CustomerID=?');
                    $cmo->bind_param('i', $cust_id);
                    $cmo->execute();
                    $cmo->store_result();
                    $cmo->bind_result($cmo_name);
                    if($cmo->num_rows > 0){
                        while($cmo->fetch()){
                            $cmo_name = strtoupper($cmo_name);
                            echo "
                                $cmo_name </br> 
                            ";
                        }
                    }
                    else{
                        echo " ";
                    }

                echo "</td>
                    <td>
                    <center>
                        <a href='javascript:void(0);' data-href='view_cmo.php?id=$cust_id' class='editCMO'><button type='button' class='btn btn-success btn-sm'><i class='fa fa-user'></i> Assign CMO</button></a>
                        <a href='javascript:void(0);' data-href='view_customer.php?id=$cust_id' class='editCustomer'><button type='button' class='btn btn-primary btn-sm'><i class='fa fa-eye'></i> Edit Details</button></a>
                        <button type='button' class='btn btn-danger btn-sm delete' id='$cust_id'><i class='fa fa-trash'></i> Delete</button>
                    </center>
                    </td>
                </tr>
                ";
            }
        }
    }

    public function show_data_dl(){
        include 'models/connection.php';
        $stmt = $con->prepare('SELECT `CompanyName` FROM `tbl_customers`');
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($cname);
        if($stmt->num_rows > 0){
            while($stmt->fetch()){
                $cname = strtoupper(str_replace('"','', $cname));
                echo "
                <option value='$cname'>
                ";
            }
        }
    }

    public function pattern(){
        include 'models/connection.php';
        $stmt = $con->prepare('SELECT `CompanyName` FROM `tbl_customers`');
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($cname);
        $pattern = '';
        if($stmt->num_rows > 0){
            while($stmt->fetch()){
                $cname = strtoupper(str_replace('"','', $cname));
                $pattern .= $cname.'|';
            }
        }

        return $pattern;
    }
    
    public function count(){
        include 'models/connection.php';
        $stmt = $con->prepare('SELECT `CustomerID` FROM `tbl_customers`');
        $stmt->execute();
        $stmt->store_result();
        return $stmt->num_rows();
    }
}
?>