<?php
include 'supplier_aed.php';

class Supplier {
    public $name;
    public $add;
    public $cperson;
    public $cnumber;

    public function set_data($id){
        include 'models/connection.php';

        $stmt = $con->prepare('SELECT `CompanyName`, `CompanyAddress`, `ContactPerson`, `ContactNumber` FROM `tbl_supplier` WHERE SupplierID=?');
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($name, $add, $cperson, $cnumber);
        $stmt->fetch();

        //assign
        $this->name = $name;
        $this->add = $add;
        $this->cperson = $cperson;
        $this->cnumber = $cnumber;
    }

    public function show_data(){
        include 'models/connection.php';
        $stmt = $con->prepare('SELECT `SupplierID`, `CompanyName`, `CompanyAddress`, `ContactPerson`, `ContactNumber` FROM `tbl_supplier` WHERE Deleted=?');
        $d = 'NO';
        $stmt->bind_param('s', $d);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($supp_id, $name, $address, $contact, $number);
        if($stmt->num_rows > 0){
            while($stmt->fetch()){
                $name = strtoupper(str_replace('"', '', $name));
                $address = strtoupper(str_replace('"', '', $address));
                $contact = str_replace('"', '', $contact);
                echo "
                <tr>
                    <td>$name</td>
                    <td>$address</td>
                    <td>$contact</td>
                    <td>$number</td>
                    <td>
                    <center>
                    <a href='javascript:void(0);' data-href='view_supplier.php?id=$supp_id' class='editSupplier'><button type='button' class='btn btn-primary btn-sm'><i class='fa fa-eye'></i> Edit Details</button></a>
                    <button type='button' class='btn btn-danger btn-sm delete' id='$supp_id'><i class='fa fa-trash'></i> Delete</button>
                    </center>
                    </td>
                </tr>
                ";
            }
        }
    }

    public function show_supplier(){
        include 'models/connection.php';
        $stmt = $con->prepare('SELECT `CompanyName` FROM `tbl_supplier` WHERE Deleted=?');
        $d = 'NO';
        $stmt->bind_param('s', $d);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($name);
        if($stmt->num_rows > 0){
            while($stmt->fetch()){
                echo "
                <option value='$name'>
                ";
            }
        }
    }
    
    public function count(){
        include 'models/connection.php';
        $stmt = $con->prepare('SELECT * FROM `tbl_supplier`');
        $stmt->execute();
        $stmt->store_result();
        return $stmt->num_rows();
    }
}
?>