<?php
include 'actprod_aed.php';

class Actual_Prod {
  /**
   * @var mixed
   */
  public $code;
  /**
   * @var mixed
   */
  public $price;
  /**
   * @var mixed
   */
  public $clevel;

  /**
   * @param $id
   */
  public function set_data($id) {
    include 'models/connection.php';

    $stmt = $con->prepare('SELECT `ProductCode`, `Price`, `CriticalLevel` FROM `tbl_actual_product` WHERE ProdCodeID=?');
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($code, $price, $clevel);
    $stmt->fetch();

    //assign
    $this->code   = $code;
    $this->price  = $price;
    $this->clevel = $clevel;
  }

  public function show_data_dl() {
    include 'models/connection.php';
    $stmt = $con->prepare('SELECT `ProductCode` FROM `tbl_product_depot`');
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($code);
    if ($stmt->num_rows > 0) {
      while ($stmt->fetch()) {
        echo "<option value='$code'>$code</option>";
      }
    }
  }

  public function show_data_my_dl() {
    include 'models/connection.php';

    $stmt = $con->prepare('SELECT `AsOfMonth` FROM `tbl_actual_prod_beg_inv` GROUP BY AsOfMonth');
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($asof);
    if ($stmt->num_rows > 0) {
      while ($stmt->fetch()) {
        echo "<option value='$asof'>$asof</option>";
      }
    }
  }

  /**
   * @param $sql
   */
  public function show_query($sql) {
    include 'models/connection.php';

    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($whname, $code, $stock, $cl);
    if ($stmt->num_rows > 0) {
      while ($stmt->fetch()) {
        echo "
                <tr>
                <td>$whname</td>
                <td>$code</td>
                <td>$stock</td>
                <td>$cl</td>
                </tr>
                ";
      }
    }
  }

  /**
   * @return mixed
   */
  public function show_code_list() {
    include 'models/connection.php';
    $stmt = $con->prepare('SELECT `ProductCode` FROM `tbl_product_depot`');
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($code);
    $pattern = '';
    if ($stmt->num_rows > 0) {
      while ($stmt->fetch()) {
        $pattern .= $code . '|';
      }
    }
    return $pattern;
  }

  public function show_data() {
    include 'models/connection.php';
    $stmt = $con->prepare('SELECT `ProductCode`, `Price`, `CriticalLevel`, ProdCodeID FROM `tbl_actual_product` WHERE Deleted=?');
    $d    = 'NO';
    $stmt->bind_param('s', $d);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($pcode, $p, $cl, $id);
    if ($stmt->num_rows > 0) {
      while ($stmt->fetch()) {
        echo "
                <tr>
                    <td>$pcode</td>
                    <td>$p</td>
                    <td>$cl</td>
                    <td>
                    <center>
                    <a href='javascript:void(0);' data-href='view_act_prod.php?id=$id' class='editProduct'><button type='button' class='btn btn-primary btn-sm'><i class='fa fa-eye'></i> Edit Details</button></a>
                    <button type='button' class='btn btn-danger btn-sm delete' id='$id'><i class='fa fa-trash'></i> Delete</button>
                    </center>
                    </td>
                </tr>
                ";
      }
    }
  }

  /**
   * @return mixed
   */
  public function count() {
    include 'models/connection.php';
    $stmt = $con->prepare('SELECT * FROM `tbl_actual_product`');
    $stmt->execute();
    $stmt->store_result();
    return $stmt->num_rows();
  }
}
?>
