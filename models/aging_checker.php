<?php
include 'connection.php';

$result = $con->query('SELECT ProductCode, CriticalLevel FROM tbl_actual_product');

while ($row = $result->fetch_assoc()) {
  if ($row['CriticalLevel'] && $row['CriticalLevel'] > 0) {
    $message  = "{$row['ProductCode']} Critical!";
    $ifExists = $con->query("SELECT * FROM tbl_notification WHERE Message='{$message}' AND Date(TimeStamp)=Date(NOW())")->num_rows > 0;

    if (!$ifExists) {
      $con->query("INSERT INTO tbl_notification (RoleID, Message) VALUES(3, '{$message}')");
    }
  }
}
?>
