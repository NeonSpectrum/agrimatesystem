<?php
if(isset($_POST['save'])){ //new record
    include 'connection.php';

    $name = $_POST['name'];
    $address = $_POST['address'];
    $cperson = $_POST['cperson'];
    $cnumber = $_POST['cnumber'];

    $stmt = $con->prepare('INSERT INTO `tbl_supplier`(`CompanyName`, `CompanyAddress`, `ContactPerson`, `ContactNumber`) VALUES (?, ?, ?, ?)');
    $stmt->bind_param('ssss', $name, $address, $cperson, $cnumber);
    if($stmt->execute()){
        header('location: ../supplier?success');
    }
}
elseif(isset($_POST['update'])){ //update record
    include 'connection.php';

    $id = $_GET['id'];
    $name = $_POST['name'];
    $address = $_POST['address'];
    $cperson = $_POST['cperson'];
    $cnumber = $_POST['cnumber'];

    $stmt = $con->prepare('UPDATE `tbl_supplier` SET `CompanyName`=?,`CompanyAddress`=?,`ContactPerson`=?,`ContactNumber`=? WHERE `SupplierID`=?');
    $stmt->bind_param('ssssi', $name, $address, $cperson, $cnumber, $id);
    if($stmt->execute()){
        header('location: ../supplier?edited');
    }
}
elseif(isset($_GET['id_delete'])){
    include 'connection.php';

    $id = $_GET['id_delete'];
    $d = 'YES';
    $stmt = $con->prepare('UPDATE `tbl_supplier` SET `Deleted`=? WHERE `SupplierID`=?');
    $stmt->bind_param('si', $d, $id);
    if($stmt->execute()){
        header('location: ../supplier?deleted');
    }
}
?>