<?php
include 'connection.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $roleID  = $_POST['roleID'];
  $message = $_POST['message'];

  addNotification($roleID, $message);
}

/**
 * @param $roleID
 * @param $message
 */
function addNotification($roleID, $message) {
  global $con;
  $con->query("INSERT INTO tbl_notification (RoleID, Message) VALUES({$roleID}, '{$message}')");
}
?>
