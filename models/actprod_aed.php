<?php
if(isset($_POST['update'])){ //update record
    include 'connection.php';

    $price = $_POST['price'];
    $clevel = $_POST['clevel'];
    $id = $_GET['id'];

    $stmt = $con->prepare('UPDATE `tbl_actual_product` SET `Price`=?,`CriticalLevel`=? WHERE `ProdCodeID`=?');
    $stmt->bind_param('dii', $price, $clevel, $id);
    if($stmt->execute()){
        header('location: ../actual_product?edited');
    }
}
elseif(isset($_GET['id_delete'])){
    include 'connection.php';

    $id = $_GET['id_delete'];
    $d = 'YES';
    $stmt = $con->prepare('UPDATE `tbl_actual_product` SET `Deleted`=? WHERE `ProdCodeID`=?');
    $stmt->bind_param('si', $d, $id);
    if($stmt->execute()){
        header('location: ../actual_product?deleted');
    }
}
?>