<script>
// window.print();
// window.onfocus=function(){ window.close();}
</script>

<?php
include 'models/connection.php';
include 'css.php';

$supplier = $_POST['supplier'];
?>

<body>
                <img src="images/logo.jpg" height="100" width="100" style="position:absolute;left:10px;top:10px" alt="">
                <center>
                  <h1>Supplier Payments Report</h1>
<?php
$filter   = [];
$filter[] = '<small>Supplier: ' . $supplier . '</small>';

echo join('<br>', $filter);
?>
                </center>
                <br>

                <table id="example2" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style='width: 16%'>Proforma Inv</th>
                            <th style='width: 16%'>Payment Terms</th>
                            <th style='width: 17%'>Total Amount to Pay</th>
                            <th style='width: 17%'>Total Deductions</th>
                            <th style='width: 17%'>Total Payments Made</th>
                            <th style='width: 17%'>Remaining Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
include 'models/connection.php';
$stmt     = $con->prepare('SELECT i.ProformaInvNo, pt.PaymentTerms, i.Total, i.Currency, i.DeductedAmount, i.Balance FROM tbl_importation i JOIN tbl_supplier s ON i.SupplierID=s.SupplierID JOIN tbl_payment_terms pt ON pt.PT_ID=i.PaymentTerm WHERE s.CompanyName LIKE ?');
$supplier = '%' . $supplier . '%';
$stmt->bind_param('s', $supplier);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($inv, $terms, $total, $currency, $ded, $bal);
if ($stmt->num_rows > 0) {
  while ($stmt->fetch()) {
    $paid = $total - $bal - $ded;

    echo "
                                <tr>
                                    <td>$inv</td>
                                    <td>$terms</td>
                                    <td>$total ($currency)</td>
                                    <td>$ded</td>
                                    <td>$paid</td>
                                    <td>$bal</td>
                                </tr>
                                ";
  }
}
?>
                    </tbody>
                </table>
</body>

<?php
include 'js.php';
?>
