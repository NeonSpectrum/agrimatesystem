<?php
include 'models/connection.php';
if (isset($_SESSION['user_id'])) {
  $stmt = $con->prepare('
    SELECT Redirection FROM tbl_users JOIN tbl_roles ON tbl_users.RoleID=tbl_roles.RoleID WHERE UserID=?
  ');
  $stmt->bind_param('s', $_SESSION['user_id']);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($redirection);
  $stmt->fetch();

  if ($stmt->num_rows > 0) {
    header('location: ./' . $redirection);
  }
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Agrimate | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/fixedColumns.bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/buttons.dataTables.min.css">
  <!-- Icon -->
  <link rel="icon" href="favicon/favicon-16x16.png">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- SWAL -->
  <script src="dist/swal/sweetalert.min.js"></script>
  <link rel="stylesheet" type="text/css" href="dist/swal/sweetalert.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
    html, body{
      height: 100% !important;
    }
    .login-sec {
      background-color: white;
      height: 400px;
      padding: 50px 20px;
    }

    .banner-sec {
      height: 400px;
      overflow: hidden;
      padding: 0;
      position: relative;
    }

    .banner-sec > .banner-text {
      position: absolute;
      font-size: 35px;
      bottom: 25px;
      left: 55px;
    }
  </style>
</head>
<body class="login-page">
  <div class="container" style="height:100%;width:75%">
    <div class="row" style="height:100%;align-items:center;justify-content:center;display:flex">
      <div class="col-md-8 banner-sec">
        <img class="img-fluid" src="images/pexels-photo.jpg" alt="First slide" style="display:block;max-width:100%;height:auto">
        <div class="banner-text"><span id="typed-banner"></span></div>
      </div>
      <div class="col-md-4 login-sec">
        <h2 class="text-center">Login Now</h2><br>
        <form action="models/login_process.php" method="POST" class="login-form">
          <div class="form-group">
            <label class="text-uppercase">Username</label>
            <input type="text" class="form-control" name="username" required>
          </div>
          <div class="form-group">
            <label class="text-uppercase">Password</label>
            <input type="password" class="form-control" name="password" required>
          </div>
          <button type="submit" name="submit" class="btn btn-login pull-right">Sign In</button>
        </form>
      </div>
    </div>
  </div>

<?php
include 'js.php';
?>
<script src="dist/js/typed.min.js"></script>
<script>
  $(function () {
    new Typed('#typed-banner', {
      strings: ['<b>Agrimate</b> Inc.'],
      typeSpeed: 50
    })
  });

<?php
if (isset($_GET['error'])) {
  ?>
    swal("Log-in Error", "You entered invalid log-in credentials.", "error");
    history.pushState(null, null, './login');
    <?php
}
?>

</script>
</body>
</html>
