<?php
require 'vendor/autoload.php';
require 'models/connection.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$title[] = 'SO #';
$title[] = 'SI #';
$title[] = 'DR #';
$title[] = 'CMO';
$title[] = 'Customer';
$title[] = 'Check Number';
$title[] = 'Provisional Receipt';
$title[] = 'Date';
$title[] = 'Total Amount';

$result = $con->query('
  SELECT * FROM tbl_sales_order
  JOIN tbl_payment
  ON tbl_sales_order.SONumber=tbl_payment.SONumber
  JOIN tbl_customers
  ON tbl_sales_order.CustomerID=tbl_customers.CustomerID
');

for ($i = 0; $row = $result->fetch_assoc(); $i++) {
  // $dateCreated = strtotime($row['DateCreated']);

  // if ($cmo && !in_array($cmo, getCMO($row['CustomerID']))
  //   || $area && $area != $row['Address']
  //   || $customer_name && $customer_name != str_replace('"', '', $row['CompanyName'])
  //   || $prod_code && $prod_code != $row['ProductCode']
  //   || $date && !($dateCreated >= $timestamp[0] && $dateCreated <= $timestamp[1])) {
  //   continue;
  // }
  //
  $data[$i][] = $row['SONumber'];
  $data[$i][] = $row['SI_Number'];
  $data[$i][] = $row['DR_Number'];
  $data[$i][] = join(', ', getCMO($row['CustomerID']));
  $data[$i][] = $row['CompanyName'];
  $data[$i][] = $row['AmountReceived'];
  $data[$i][] = date('F d, Y', strtotime($row['CheckDate']));
  $data[$i][] = $row['PR_Number'];
  $data[$i][] = $row['CheckNumber'];
}

$spreadsheet = new Spreadsheet();
$sheet       = $spreadsheet->getActiveSheet();

for ($i = 0, $cell = 'A'; $i < count($title); $i++, $cell++) {
  $sheet->setCellValue($cell . '1', $title[$i]);
}

for ($i = 0; $i < count($data); $i++) {
  if (isset($data[$i])) {
    for ($j = 0, $cell = 'A'; $j < count($data[$i]); $j++, $cell++) {
      $sheet->setCellValue($cell . ($i + 2), $data[$i][$j]);
    }
  }
}

foreach (range('A', $sheet->getHighestDataColumn()) as $col) {
  $sheet->getStyle($col . '1')->getFont()->setBold(true);
  $sheet
    ->getColumnDimension($col)
    ->setAutoSize(true);
}

$writer = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . date('F_d_Y_h_i_s_A') . ' - Invoice Report.xlsx"');
$writer->save('php://output');

/**
 * @param $id
 */
function getCMO($id) {
  global $con;

  $list = [];

  $result = $con->query("SELECT * FROM `tbl_cmo` JOIN tbl_cmo_cust ON tbl_cmo.CMO_ID=tbl_cmo_cust.CMO_ID WHERE CustomerID='{$id}'");

  while ($row = $result->fetch_assoc()) {
    $list[] = $row['FullName'];
  }

  return $list;
}
?>
