<script>
// window.print();
// window.onfocus=function(){ window.close();}
</script>

<?php
include 'models/connection.php';
include 'css.php';

$warehouse = $_GET['warehouse'] ?? null;
$prodcode  = $_GET['prodcode'] ?? null;
$date      = $_GET['date'] ? explode(' - ', $_GET['date']) : null;

if ($date) {
  $timestamp[0] = strtotime($date[0]);
  $timestamp[1] = strtotime($date[1]);
}
?>

<body>
                <img src="images/logo.jpg" height="100" width="100" style="position:absolute;left:10px;top:10px" alt="">
                <center>
                  <h1>Inventory Report</h1>
<?php
$filter = [];

if ($warehouse) {
  $filter[] = '<small>Warehouse: ' . $warehouse . '</small>';
}
if ($prodcode) {
  $filter[] = '<small>Product Code: ' . $prodcode . '</small>';
}
if ($date) {
  $filter[] = '<small>From: ' . date('F d, Y', $timestamp[0]) . ' To: ' . date('F d, Y', $timestamp[1]) . '</small>';
}

echo join('<br>', $filter);

$total = 0;
?>
                </center>
                <br>

                <table id="example2" class="table table-bordered table-striped">
                    <!-- table-bordered table-striped-->
                  <thead>
                    <tr>
                      <th style='text-align:center; vertical-align:middle'>Action</th>
                      <th style='text-align:center; vertical-align:middle'>Transfer Date</th>
                      <th style='text-align:center; vertical-align:middle'>STF#</th>
<?php
if (!$prodcode):
?>
                      <th style='text-align:center; vertical-align:middle'>Product Code</th>
<?php endif;?>
                      <th style='text-align:center; vertical-align:middle'>Origin</th>
                      <th style='text-align:center; vertical-align:middle'>Destination</th>
                      <th style='text-align:center; vertical-align:middle'>Qty</th>
                    </tr>
                  </thead>
                  <tbody>
<?php
$previousMonthTime = strtotime('last day of previous month', $timestamp[0] ?? time());
$previousMonth     = date('F Y', $previousMonthTime);
?>
                    <tr>
                      <td>Beginning Inventory (<?php echo date('F d, Y', $previousMonthTime); ?>)</td>
                      <td></td>
                      <td></td>
<?php
if (!$prodcode):
?>
                      <td></td>
<?php endif;?>
                      <td></td>
                      <td></td>
                      <td align="right">
<?php
echo getBeginningInv($warehouse, $prodcode, $previousMonth);
?>
                      </td>
                    </tr>
<?php
$subtotal = 0;
$result   = $con->query('SELECT * FROM tbl_transfer JOIN tbl_transferred_prod ON tbl_transfer.TransferID=tbl_transferred_prod.TransferID');
while ($row = $result->fetch_assoc()) {
  $fromWarehouse   = getWarehouseName($row['FromWarehouse']);
  $toWarehouse     = getWarehouseName($row['ToWarehouse']);
  $currentProdcode = $row['ProductCode'];
  $transferDate    = strtotime($row['TransferDate']);

  if ($date && !($transferDate >= $timestamp[0] && $transferDate <= $timestamp[1])
    || $warehouse && $fromWarehouse != $warehouse && $toWarehouse != $warehouse
    || $prodcode && $prodcode != $currentProdcode) {
    continue;
  }

  if ($warehouse && $warehouse == $fromWarehouse) {
    $row['Quantity'] = $row['Quantity'] * -1;
  }

  $subtotal += $row['Quantity'];
  ?>
                    <tr>
                      <td>Transfer</td>
                      <td align="center"><?php echo date('F d, Y', $transferDate); ?></td>
                      <td align="right"><?php echo $row['TransferID']; ?></td>
<?php
if (!$prodcode):
  ?>
                      <td align="center"><?php echo $row['ProductCode']; ?></td>
<?php endif;?>
                      <td align="center"><?php echo getWarehouseName($row['FromWarehouse']); ?></td>
                      <td align="center"><?php echo getWarehouseName($row['ToWarehouse']); ?></td>
                      <td align="right"><?php echo $row['Quantity']; ?></td>
                    </tr>
<?php
}
?>
                    <tr>
                      <th colspan="<?php echo !$prodcode ? 6 : 5; ?>" style="text-align:right">Subtotal:</th>
                      <th style="text-align:right"><?php echo number_format($subtotal, 0, '.', ','); ?></th>
                    </tr>
                    <tr>
                      <th style='text-align:center; vertical-align:middle'>Action</th>
                      <th style='text-align:center; vertical-align:middle'>Delivery Date</th>
                      <th style='text-align:center; vertical-align:middle'>DR #</th>
<?php
if (!$prodcode):
?>
                      <th style='text-align:center; vertical-align:middle'>Product Code</th>
<?php endif;?>
                      <th style='text-align:center; vertical-align:middle'>Origin</th>
                      <th style='text-align:center; vertical-align:middle'>Customer Name</th>
                      <th style='text-align:center; vertical-align:middle'>Qty</th>
                    </tr>
<?php
$total += $subtotal;
$subtotal = 0;
$result   = $con->query('SELECT * FROM tbl_sales_order JOIN tbl_so_product ON tbl_sales_order.SONumber=tbl_so_product.SO_ID JOIN tbl_customers ON tbl_sales_order.CustomerID=tbl_customers.CustomerID JOIN tbl_warehouse ON tbl_sales_order.WarehouseID=tbl_warehouse.WarehouseID');
while ($row = $result->fetch_assoc()) {
  $dateCreated     = strtotime($row['DateCreated']);
  $currentProdcode = $row['ProductCode'];

  if ($date && !($dateCreated >= $timestamp[0] && $dateCreated <= $timestamp[1])
    || !$row['DR_Number']
    || $prodcode && $prodcode != $currentProdcode) {
    continue;
  }
  $row['Quantity'] = $row['Quantity'] * -1;
  $subtotal += $row['Quantity'];
  ?>
                    <tr>
                      <td>Delivery</td>
                      <td align="center"><?php echo date('F d, Y', $dateCreated); ?></td>
                      <td align="right"><?php echo $row['DR_Number']; ?></td>
                      <td align="center"><?php echo $row['ProductCode']; ?></td>
                      <td align="center"><?php echo $row['WarehouseName']; ?></td>
                      <td align="center"><?php strtoupper(str_replace('"', '', $row['CompanyName']));?></td>
                      <td align="right"><?php echo $row['Quantity']; ?></td>
                    </tr>
<?php
}
?>
                    <tr>
                      <th colspan="5" style="text-align:right">Subtotal:</th>
                      <th style="text-align:right"><?php echo number_format($subtotal, 0, '.', ','); ?></th>
                    </tr>
                    <tr>
                      <th style='text-align:center; vertical-align:middle'>Action</th>
                      <th style='text-align:center; vertical-align:middle'>Breakdown Date</th>
                      <th style='text-align:center; vertical-align:middle'>-</th>
<?php
if (!$prodcode):
?>
                      <th style='text-align:center; vertical-align:middle'>Product Code</th>
<?php endif;?>
                      <th style='text-align:center; vertical-align:middle'>Origin</th>
                      <th style='text-align:center; vertical-align:middle'>Destination</th>
                      <th style='text-align:center; vertical-align:middle'>Qty</th>
                    </tr>
<?php
if (!$warehouse || $warehouse == 'MANILA') {
  $total += $subtotal;
  $subtotal = 0;
  $result   = $con->query('SELECT * FROM tbl_imp_breakdown');

  $hasOutput = false;

  while ($row = $result->fetch_assoc()) {
    $dateCreated     = strtotime($row['DateCreated']);
    $currentProdcode = $row['ProductCode'];

    if ($date && !($dateCreated >= $timestamp[0] && $dateCreated <= $timestamp[1])
      || $prodcode && $prodcode != $currentProdcode) {
      continue;
    }

    $hasOutput = true;
    $subtotal += $row['Quantity'];
    ?>
                    <tr>
                      <td>Importation</td>
                      <td align="center"><?php echo date('F d, Y', $dateCreated) ?></td>
                      <td></td>
<?php
if (!$prodcode):
    ?>
                      <td align="center"><?php echo $row['ProductCode']; ?></td>
<?php endif;?>
                      <td></td>
                      <td></td>
                      <td align="right"><?php echo $row['Quantity'] ?></td>
                    </tr>
<?php
}
  $total += $subtotal;
  if ($hasOutput) {
    ?>
                    <tr>
                      <th colspan="<?php echo !$prodcode ? 6 : 5; ?>" style="text-align:right">Subtotal:</th>
                      <th style="text-align:right"><?php echo number_format($subtotal, 0, '.', ','); ?></th>
                    </tr>
                    <tr>
                      <th colspan="<?php echo !$prodcode ? 6 : 5; ?>"></th>
                    </tr>
                    <tr>
                      <th colspan="<?php echo !$prodcode ? 6 : 5; ?>" style="text-align:right">TOTAL:</th>
                      <th style="text-align:right"><?php echo number_format($total, 0, '.', ','); ?></th>
                    </tr>
<?php
}
}
?>
                  </tbody>
                </table>


</body>

<?php
include 'js.php';
?>

<?php
/**
 * @param $id
 * @return mixed
 */
function getWarehouseName($id) {
  global $con;
  return $con->query("SELECT WarehouseName FROM tbl_warehouse WHERE WarehouseID='{$id}'")->fetch_assoc()['WarehouseName'];
}

/**
 * @param $wh
 * @param $prodcode
 * @param $asof
 */
function getBeginningInv($wh, $prodcode = null, $asof) {
  global $con;

  $subtotal = 0;

  if ($prodcode) {
    $stmt = $con->prepare('SELECT `CurrentStock` FROM `tbl_actual_prod_beg_inv` ap JOIN tbl_warehouse w ON w.WarehouseID=ap.WarehouseID WHERE w.WarehouseName=? AND ProductCode=? AND AsOfMonth=?');
    $stmt->bind_param('sss', $wh, $prodcode, $asof);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($stock);
    if ($stmt->num_rows > 0) {
      while ($stmt->fetch()) {
        $subtotal += $stock;
      }
    }
  } else {
    $stmt = $con->prepare('SELECT `CurrentStock` FROM `tbl_actual_prod_beg_inv` ap JOIN tbl_warehouse w ON w.WarehouseID=ap.WarehouseID WHERE w.WarehouseName=? AND AsOfMonth=?');
    $stmt->bind_param('ss', $wh, $asof);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($stock);
    if ($stmt->num_rows > 0) {
      while ($stmt->fetch()) {
        $subtotal += $stock;
      }
    }
  }

  return $subtotal;
}
?>
