<script>
// window.print();
// window.onfocus=function(){ window.close();}
</script>

<?php
include 'models/connection.php';
include 'css.php';

$date = $_POST['date'];
?>

<body>
                <img src="images/logo.jpg" height="100" width="100" style="position:absolute;left:10px;top:10px" alt="">
                <center>
                  <h1>Payments Report</h1>
<?php
$filter   = [];
$filter[] = '<small>Date: ' . $date . '</small>';

echo join('<br>', $filter);
?>
                </center>
                <br>

                <table id="example2" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style='width: 14%'>Proforma Inv</th>
                            <th style='width: 16%'>Supplier</th>
                            <th style='width: 14%'>Bank Name</th>
                            <th style='width: 14%'>Amount Paid</th>
                            <th style='width: 14%'>Bank Charges</th>
                            <th style='width: 14%'>Bank Rate</th>
                            <th style='width: 14%'>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
include 'models/connection.php';

$date  = explode(' - ', $date);
$start = date_format(date_create($date[0]), 'Y-m-d');
$end   = date_format(date_create($date[1]), 'Y-m-d');

$stmt = $con->prepare('SELECT py.ProformaInvNo, s.CompanyName, `Bank`, `AmountPaid`, i.Currency, `BankCharges`, `Rate`, `PHPAmount` FROM `tbl_paid` pd JOIN tbl_payables py ON py.PayableID=pd.PayableID JOIN tbl_importation i ON i.ProformaInvNo=py.ProformaInvNo JOIN tbl_supplier s ON s.SupplierID=i.SupplierID WHERE pd.DatePaid BETWEEN ? AND ?');
$stmt->bind_param('ss', $start, $end);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($inv, $company, $bank, $amt, $currency, $chg, $rate, $phpamt);
if ($stmt->num_rows > 0) {
  while ($stmt->fetch()) {
    echo "
                                <tr>
                                    <td>$inv</td>
                                    <td>$company</td>
                                    <td>$bank</td>
                                    <td>$amt ($currency)</td>
                                    <td>$chg</td>
                                    <td>$rate</td>
                                    <td>$phpamt</td>
                                </tr>
                                ";
  }
}
?>
                    </tbody>
                </table>
</body>

<?php
include 'js.php';
?>
