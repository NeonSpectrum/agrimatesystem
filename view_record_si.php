<?php
include 'models/payment_model.php';
include 'models/credit_model.php';
include 'models/rebate_model.php';
include 'models/debit_model.php';
include 'models/sales_order_model.php';

$so_ = new Sales_Order();
$payment = new Payment();
$id      = $_GET['si'];
$so = $_GET['so'];

$so_->set_data($so);

$payment->get_total_payment($id);

$rebate = new Rebate();
$credit = new Credit();
$debit  = new Debit();

$bal = $so_->balance;
$amt = $so_->total;

if ($bal <= 0) {
  $x = '(paid)';
}
elseif($bal > 0 && $bal < $amt){
  $x = '(partially paid)';
}
else {
  $x = '';
}

$total_rebate = $rebate->total_rebate($id);
$total_credit = $credit->total_credit($id);
$total_debit = $debit->total_debit($id);
$net = $amt + $total_debit - $total_rebate - $total_credit;
?>
                    <!-- /.card-header -->
              <div class="card-body">

              <div class="col-md-6" style="float:left">
              <div class="form-group">
                  <label>Invoice Amount</label>
                  <input type='text' name="si_number" class="form-control" readonly value='<?php echo $amt," ", $x; ?>'>
              </div>
              </div>

              <div class="col-md-6" style="float:left">
              <div class="form-group">
                  <label>Net Sale</label>
                  <input type='text' name="si_number" class="form-control" readonly value='<?php echo number_format($net,2); ?>'>
              </div>
              </div>

              <table id="example2" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                      <th style='width:15%'>PR #</th>
                      <th style='width:15%'>CR #</th>
                      <th style='width:25%'>Bank</th>
                      <th style='width:15%'>Check Number</th>
                      <th style='width:15%'>Receive Date</th>
                      <th style='width:15%'>Amount Paid</th>
                    </tr>
                  </thead>
                  <tbody id="prod_table1">
                      <?php
$payment->show_data($id);
?>
                  </tbody>
                </table>
                <br>
                <table id="example2" style='width: 80%; margin: 0 auto' class="table table-bordered table-striped">
                  <thead>
                  <tr>
                      <th style='width:50%'>Miscellaneous Fees</th>
                      <th style='width:50%'>Amount</th>
                    </tr>
                  </thead>
                  <tbody id="prod_table1">
                      <?php
$payment->show_misc($id);
?>
                  </tbody>
                </table>
                <br>
                <table id="example3" style='width: 80%; margin: 0 auto' class="table table-bordered table-striped">
                  <thead>
                  <tr>
                      <th style='width:20%'>Rebate Number</th>
                      <th style='width:40%'>Rebate Date</th>
                      <th style='width:40%'>Amount</th>
                    </tr>
                  </thead>
                  <tbody id="prod_table1">
                      <?php
$rebate->show_data($id);
?>
                  </tbody>
                </table>
                <br>
                <table id="example3" style='width: 80%; margin: 0 auto' class="table table-bordered table-striped">
                  <thead>
                  <tr>
                      <th style='width:20%'>Credit Memo Number</th>
                      <th style='width:40%'>Credit Date</th>
                      <th style='width:40%'>Amount</th>
                    </tr>
                  </thead>
                  <tbody id="prod_table1">
                      <?php
$credit->show_data($id);
?>
                  </tbody>
                </table>
                <br>
                <table id="example3" style='width: 80%; margin: 0 auto' class="table table-bordered table-striped">
                  <thead>
                  <tr>
                      <th style='width:20%'>Debit Memo Number</th>
                      <th style='width:40%'>Debit Date</th>
                      <th style='width:40%'>Amount</th>
                    </tr>
                  </thead>
                  <tbody id="prod_table1">
                      <?php
$debit->show_data($id);
?>
                  </tbody>
                </table>
                <br>
                <br>
              </div>
              <!-- /.card-body -->
