<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
<?php if ($role->has('admin')): ?>
      <li>
        <a href="records">
          <i class="fa fa-dashboard"></i> <span>Records Management</span>
        </a>
      </li>
<?php endif;?>
<?php if ($role->has('importation')): ?>
      <li>
        <a href="importation">
          <i class="fa fa-plane"></i> <span>Importation</span>
        </a>
      </li>
<?php endif;?>
<?php if ($role->has('sales', 'importation')): ?>
      <li class='treeview'>
        <a href="#">
          <i class="fa fa-archive"></i> <span>Inventory</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
<?php if ($role->has('sales')): ?>
          <li><a href="inv_breakdown"><i class="fa fa-circle-o"></i> Products</a></li>
<?php endif;?>
          <li><a href="inv_monitoring"><i class="fa fa-circle-o"></i> Monitor</a></li>
<?php if ($role->has('sales')): ?>
          <li><a href="inv_transfer"><i class="fa fa-circle-o"></i> Transfer</a></li>
          <li><a href="inv_report"><i class="fa fa-circle-o"></i> Reports</a></li>
<?php endif;?>
        </ul>
      </li>
<?php endif;?>
<?php if ($role->has('sales')): ?>
      <li>
        <a href="sales">
          <i class="fa fa-credit-card"></i> <span>Sales and Marketing</span>
        </a>
      </li>
<?php endif;?>
<?php if ($role->has('billing', 'credit', 'importation', 'sales')): ?>
      <li class='treeview'>
        <a href="#">
          <i class="fa fa-address-book"></i> <span>SO Records</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
<?php if ($role->has('credit', 'sales')): ?>
          <li><a href="so_records"><i class="fa fa-circle-o"></i> Pending SO Records</a></li>
<?php endif;?>
<?php if ($role->has('importation', 'sales', 'credit', 'billing')): ?>
          <li><a href="approved_records"><i class="fa fa-circle-o"></i> Approved SO Records</a></li>
<?php endif;?>
<?php if ($role->has('credit')): ?>
          <li><a href="disapproved_records"><i class="fa fa-circle-o"></i> Disapproved SO Records</a></li>
<?php endif;?>
<?php if ($role->has('credit')): ?>
          <li><a href="cancelled_records"><i class="fa fa-circle-o"></i> Cancelled SO Records</a></li>
<?php endif;?>
        </ul>
      </li>
<?php endif;?>
<?php if ($role->has('credit', 'finance')): ?>
      <li>
        <a href="payments">
          <i class="fa fa-money"></i> <span>Credit and Collection</span>
        </a>
      </li>
<?php endif;?>
<?php if ($role->has('billing', 'credit')): ?>
      <li>
        <a href="sidr_records">
          <i class="fa fa-money"></i> <span>Billing</span>
        </a>
      </li>
<?php endif;?>
<?php if ($role->has('accounting')): ?>
      <li>
        <a href="accounting">
          <i class="fa fa-book"></i> <span>Accounting</span>
        </a>
      </li>
<?php endif;?>
<?php if ($role->has('finance', 'finance-2', 'importation')): ?>
      <li class='treeview'>
        <a href="#">
          <i class="fa fa-money"></i> <span>Finance</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
<?php if ($role->has('importation', 'finance', 'finance-2')): ?>
          <li><a href="finance"><i class="fa fa-circle-o"></i> Payments for Importation</a></li>
<?php endif;?>
<?php if ($role->has('finance', 'finance-2')): ?>
          <li><a href="check"><i class="fa fa-circle-o"></i> Check Voucher</a></li>
<?php endif;?>
<?php if ($role->has('finance', 'finance-2')): ?>
          <li><a href="check_approval"><i class="fa fa-circle-o"></i> Check Voucher Approval</a></li>
<?php endif;?>
        </ul>
      </li>
<?php endif;?>
<?php if ($role->has('admin', 'credit')): ?>
      <li>
        <a href="reports">
          <i class="fa fa-bar-chart"></i> <span>Reports</span>
        </a>
      </li>
<?php endif;?>
    </ul>
  </section>
</aside>
