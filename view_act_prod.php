<?php
if(isset($_GET['id'])){
    $id = $_GET['id'];

    include 'models/actual_prod_model.php';
    $ap = new Actual_Prod();

    $ap->set_data($id);
    $code = $ap->code;
    $price = $ap->price;
    $clevel = $ap->clevel;
}
?>

<style>
.loader {
  border: 4px solid #f3f3f3;
  border-radius: 50%;
  border-top: 4px solid #3498db;
  width: 12px;
  height: 12px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>

                    <form action='models/actual_prod_model.php?id=<?php echo $id; ?>' method='post'>  
                  <div class="form-group">
                    <div style='width:100%'>
                    <label>Product Code</label>
                    </div>
                    
                    <input type='text' maxlength='200' readonly id='name_input' value='<?php echo $code; ?>' required name="name" class="form-control">
                  </div>

                  <div class="form-group">
                    <label>Price</label>
                    <input type='number' step='any' min='0' max='1000000' value='<?php echo $price; ?>' name="price" class="form-control" placeholder='Type input here...'>
                  </div>

                  <div class="form-group">
                    <label>Critical Level</label>
                    <input type='number' min='0' max='1000000' value='<?php echo $clevel; ?>' name="clevel" class="form-control" placeholder='Type input here...'>
                  </div>

                  <button type="submit" 
                  name='update' id='submit' class="btn btn-success" style="float:right; margin-top:2px; margin-right:10px"><i class='fa fa-save'></i> &nbsp;Save Details</button>
                   </form>
